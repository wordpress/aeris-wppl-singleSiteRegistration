<?php 
/**
* Plugin Name: Aeris Single site registration
* Plugin URI : https://git.icare.univ-lille1.fr/wordpress/aeris-wppl-singleSiteRegistration
* Text Domain: aeris-wppl-singleSiteRegistration
* Domain Path: /languages
* Description: Manage Single site registration on multisite
* Author: Pierre VERT
* Version: 1.0.0
* GitHub Plugin URI: wordpress/aeris-wppl-singleSiteRegistration
* GitHub Branch:     master
*/
 
add_filter( 'wp_signup_location', 'mbdr_current_blog_home' );
function mbdr_current_blog_home($location)
{
    return get_site_url();
}
 
add_action('before_signup_header', 'mbdr_redirect_signup_home');
function mbdr_redirect_signup_home(){
    wp_redirect( get_site_url() );
    exit();
}

?>